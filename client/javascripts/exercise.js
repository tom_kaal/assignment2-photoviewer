/*global window*/
/*global document*/
/*global photoViewer*/
/*global console*/
/*global alert*/
/*global setTimeout*/
/*global YoutubeVideo*/
/*global myVid*/
/*global cheatText*/
/*jshint -W031 */
(function () {
    "use strict";

    window.photoViewer = {

        settings: {
            proxy: "http://server7.tezzt.nl:1332/api/proxy",
            rows: 3,
            cols: 3
        },
        tiles: [],
        tArr: [],
        cheatMode: false,
        isPlaying: true,
        mute: false,


        createTiles: function () {
            var tileBox, rowCounter, columnCounter, tileName, tileWidth, tileHeight, tileContainer, bodyNode;
            tileWidth = Math.floor(document.querySelector("#myVid").offsetWidth / photoViewer.settings.cols);
            tileHeight = Math.floor(document.querySelector("#myVid").offsetHeight / photoViewer.settings.rows);

            bodyNode = document.querySelector("body");

            //tile container
            tileContainer = document.createElement("div");
            tileContainer.className = "tileContainer";
            tileContainer.style.width = Math.floor(document.querySelector("#myVid").offsetWidth + photoViewer.settings.cols * 2) + "px";
            tileContainer.style.height = Math.floor(document.querySelector("#myVid").offsetHeight + photoViewer.settings.rows * 2) + "px";


            bodyNode.appendChild(tileContainer);

            for (columnCounter = 0; columnCounter < photoViewer.settings.cols; columnCounter += 1) {

                for (rowCounter = 0; rowCounter < photoViewer.settings.rows; rowCounter += 1) {
                    tileName = "tile" + rowCounter + columnCounter;
                    tileBox = document.createElement("canvas");
                    tileBox.id = tileName;
                    tileBox.width = tileWidth;
                    tileBox.height = tileHeight;
                    tileBox.className = "tile";
                    tileContainer.appendChild(tileBox);

                    document.getElementById("tile" + rowCounter + columnCounter).addEventListener("click", photoViewer.clickHandler);



                }
            }
            photoViewer.updateTiles();
        },

        getTileIndex: function (x, y) {
            var rows, getal, intX;
            rows = photoViewer.settings.rows;
            intX = parseInt(x, 10);
            getal = (y * rows) + intX;
            return getal;
        },
        maySwitch: function (index1, object2) {
            var multiplyByColumn, index2, rowUpDownNumber;
            multiplyByColumn = object2.column;
            index2 = photoViewer.getTileIndex(object2.row, object2.column);
            rowUpDownNumber = photoViewer.settings.rows;
            if (index1 - rowUpDownNumber === index2 || index1 + rowUpDownNumber === index2 || index1 + 1 === index2 || index1 - 1 === index2) {
                if (!((index2 === rowUpDownNumber * multiplyByColumn && index1 + 1 === index2) || (index2 === ((rowUpDownNumber - 1) + (multiplyByColumn * rowUpDownNumber)) && index1 - 1 === index2))) {
                    return true;
                }
            }
            if (photoViewer.cheatMode === true) {
                return true;
            }

        },

        clickHandler: function (tilebox) {
            var string, numbers, sNumber, xLat, yLat, i;
            string = tilebox.srcElement.id;
            numbers = string.slice(4);
            sNumber = numbers.split("");
            xLat = sNumber[0];
            yLat = sNumber[1];

            for (i = 0; i < photoViewer.tiles.length; i += 1) {
                if (photoViewer.tiles[i].name === tilebox.srcElement.id) {
                    photoViewer.switchTiles(photoViewer.tiles[i], photoViewer.getTileIndex(xLat, yLat));
                    i = photoViewer.tiles.length;

                }
            }


        },

        arrowKeyHandler: function (otherTileIndex) {

            var z, x, y, i;
            for (i = 0; i < photoViewer.tiles.length; i += 1) {

                if (photoViewer.tiles[i].blockedImage === true) {
                    z = i + otherTileIndex;
                    y = photoViewer.tiles[z].column;
                    x = photoViewer.tiles[z].row;
                    console.log(y, x);
                    photoViewer.switchTiles(photoViewer.tiles[z], z, true);
                    i = photoViewer.tiles.length;

                }
            }
        },

        isSolved: function () {
            var i, teller, x, y;
            teller = 0;

            for (i = 0; i < photoViewer.tiles.length; i += 1) {
                y = photoViewer.tiles[i].column;
                x = photoViewer.tiles[i].row;

                if (photoViewer.getTileIndex(x, y) === i) {
                    teller += 1;
                }
            }
            if (teller === photoViewer.tiles.length) {
                return true;
            }
        },

        switchTiles: function (tileToSwitch, tileIndex, arrow) {
            var i, tempTile;
            for (i = 0; i < photoViewer.tiles.length; i += 1) {

                if (photoViewer.tiles[i].blockedImage === true) {
                    if (photoViewer.maySwitch(i, tileToSwitch) === true || arrow === true) {
                        tempTile = {};

                        tempTile.blockedImage = photoViewer.tiles[i].blockedImage;
                        tempTile.column = photoViewer.tiles[i].column;
                        tempTile.display = photoViewer.tiles[i].display;
                        tempTile.name = photoViewer.tiles[i].name;
                        tempTile.row = photoViewer.tiles[i].row;
                        tempTile.sequence = photoViewer.tiles[i].sequence;

                        photoViewer.tiles[i] = photoViewer.tiles[tileIndex];
                        photoViewer.tiles[tileIndex] = tempTile;

                        i = photoViewer.tiles.length;

                        if (photoViewer.isSolved() === true) {
                            photoViewer.tiles[i - 1].blockedImage = false;
                            alert("OPGELOST!");
                        }
                    }
                }
            }
        },


        updateTiles: function () {
            var tileWidth, tileHeight, tileCounter, videoNode, imgNode, columnCounter, rowCounter, tile, row, column, tileName, tileNode, ctx;
            tileWidth = Math.floor(document.querySelector("#myVid").offsetWidth / photoViewer.settings.cols);
            tileHeight = Math.floor(document.querySelector("#myVid").offsetHeight / photoViewer.settings.rows);

            tileCounter = 0;
            videoNode = document.querySelector("#myVid");
            imgNode = document.querySelector("#emptyTile");

            for (columnCounter = 0; columnCounter < photoViewer.settings.cols; columnCounter += 1) {

                for (rowCounter = 0; rowCounter < photoViewer.settings.rows; rowCounter += 1) {

                    tile = photoViewer.tiles[tileCounter];
                    row = tile.row;
                    column = tile.column;
                    tileName = "tile" + rowCounter + columnCounter;

                    tileNode = document.querySelector("#" + tileName);


                    ctx = tileNode.getContext("2d");

                    if (tile.blockedImage === true) {
                        ctx.drawImage(imgNode,
                            0, 0, 256, 256,
                            0, 0, tileWidth, tileHeight);
                    } else {
                        ctx.drawImage(videoNode,
                            tileWidth * row, tileHeight * column, tileWidth, tileHeight,
                            0, 0, tileWidth, tileHeight);
                    }




                    tileCounter = tileCounter + 1;

                }
            }
            setTimeout(photoViewer.updateTiles, 33);


        },

        getImage: function () {
            var imgNode = document.createElement("img");
            imgNode.src = "http://static.parastorage.com/media/af26f0_0bef7d3b29f849b7ae2a35b641f43eae.jpg_256";
            imgNode.alt = "";
            imgNode.id = "emptyTile";
            imgNode.style.display = "none";
            document.body.appendChild(imgNode);
        },

        randomizeTiles: function () {
            var cells, t, i;
            cells = photoViewer.settings.cols * photoViewer.settings.rows;
            t = [];

            for (i = 0; i < cells; i = i + 1) {
                t.push(photoViewer.tiles[photoViewer.tArr[i]]);

            }
            photoViewer.tiles = t;
        },


        createVideoNode: function () {
            var bodyNode, videoNode, youtubeId, cb, webm, mp4;

            bodyNode = document.querySelector("body");

            videoNode = document.createElement("video");
            videoNode.id = "myVid";
            bodyNode.appendChild(videoNode);


            youtubeId = "qPd1YWTSwE0";
            cb = function (video) {
                console.log(video.title);
                webm = video.getSource("video/webm", "medium");
                console.log("WebM: " + webm.url);
                mp4 = video.getSource("video/mp4", "medium");
                console.log("MP4: " + mp4.url);
                videoNode.src = webm.url;
                videoNode.addEventListener("loadeddata", photoViewer.createTiles);

            };
            YoutubeVideo(youtubeId, photoViewer.settings.proxy, cb);
            myVid.play();


        },

        init: function () {
            var h1Node, bodyNode, pNode, tileCounter, rowCounter, columnCounter, tile, randomNumberCounter, r;

            bodyNode = document.querySelector("body");
            h1Node = document.createElement("h1");
            h1Node.textContent = "Video sliding puzzle";
            bodyNode.appendChild(h1Node);

            pNode = document.createElement("p");
            pNode.textContent = "User the mouse or arrow keys to move the tiles. Press P to pause and play. Press C to enable cheatmode so the tiles can be switched wherever they are positioned. Press M to enable/disable the music";

            bodyNode.appendChild(pNode);

            photoViewer.getImage();

            //initialize array
            tileCounter = 0;

            for (rowCounter = 0; rowCounter < photoViewer.settings.rows; rowCounter += 1) {
                for (columnCounter = 0; columnCounter < photoViewer.settings.cols; columnCounter += 1) {
                    if ((columnCounter + 1) * (rowCounter + 1) - 1 === (photoViewer.settings.cols * photoViewer.settings.rows) - 1) {
                        tile = {
                            name: "tile" + rowCounter + columnCounter,
                            display: true,
                            sequence: tileCounter,
                            row: rowCounter,
                            column: columnCounter,
                            blockedImage: true


                        };
                    } else {
                        tile = {
                            name: "tile" + rowCounter + columnCounter,
                            display: true,
                            sequence: tileCounter,
                            row: rowCounter,
                            column: columnCounter,
                            blockedImage: false

                        };
                    }

                    photoViewer.tiles.push(tile);

                    tileCounter += 1;
                }
            }
            console.log(photoViewer.tiles);

            while (photoViewer.tArr.length !== photoViewer.tiles.length) {
                for (randomNumberCounter = 0; randomNumberCounter < photoViewer.tiles.length; randomNumberCounter += 1) {
                    r = photoViewer.r(0, photoViewer.tiles.length);
                    if (!photoViewer.contains(r, photoViewer.tArr)) {
                        photoViewer.tArr.push(r);
                    }
                }
            }
            photoViewer.randomizeTiles();
        },

        r: function (min, max) {
            return (Math.floor((Math.random() * (max - min) + min)));
        },

        contains: function (el, ar) {
            var contains, i;
            contains = false;
            for (i = 0; i < ar.length; i += 1) {
                if (el === ar[i]) {
                    contains = true;
                }
            }
            return contains;
        },


        run: function () {
            photoViewer.init();
            photoViewer.createVideoNode();
        },

        changePlayingState: function () {
            if (photoViewer.isPlaying === true) {
                myVid.play();
            } else if (photoViewer.isPlaying === false) {
                myVid.pause();
            }
        },

        showCheatmode: function () {
            var bodyNode, h5Node;
            bodyNode = document.querySelector("body");

            if (photoViewer.cheatMode === true) {
                h5Node = document.createElement("h5");
                h5Node.id = "cheatText";
                h5Node.textContent = "CheatMode enabled";
                bodyNode.appendChild(h5Node);
            } else {

                bodyNode.removeChild(cheatText);
            }
        },
        mutefunction: function () {
            if (photoViewer.mute === true) {
                myVid.muted = true;
            } else {
                myVid.muted = false;
            }
        }


    };


}());

window.addEventListener("load", photoViewer.run);

window.onkeydown = function (e) {
    "use strict";

    var up, down, left, right, cheatMode, pausePlay, mainProgram, mute, code;
    up = 38;
    down = 40;
    left = 37;
    right = 39;
    cheatMode = 67;
    pausePlay = 80;
    mute = 77;
    mainProgram = window.photoViewer;
    code = e.keyCode || e.which;
    if (code === up) { //up key
        mainProgram.arrowKeyHandler(-mainProgram.settings.cols);
    } else if (code === down) { //down key
        mainProgram.arrowKeyHandler(mainProgram.settings.cols);
    } else if (code === left) { //left key
        mainProgram.arrowKeyHandler(-1);
    } else if (code === right) { //right key
        mainProgram.arrowKeyHandler(1);
    } else if (code === cheatMode) { //C key
        mainProgram.cheatMode = !mainProgram.cheatMode;
        mainProgram.showCheatmode();
        console.log(mainProgram.cheatMode);
    } else if (code === pausePlay) { //P key
        mainProgram.isPlaying = !mainProgram.isPlaying;
        mainProgram.changePlayingState();
    } else if (code === mute) {
        mainProgram.mute = !mainProgram.mute;
        mainProgram.mutefunction();
    }
};